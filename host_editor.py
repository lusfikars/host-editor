import wx
import wx.lib.mixins.listctrl as listmix
import re
import os

class TestListCtrl(wx.ListCtrl, listmix.CheckListCtrlMixin, listmix.ListCtrlAutoWidthMixin, listmix.TextEditMixin):
	def __init__(self, *args, **kwargs):
		wx.ListCtrl.__init__(self, *args, **kwargs)
		listmix.CheckListCtrlMixin.__init__(self)
		listmix.TextEditMixin.__init__(self)
		listmix.ListCtrlAutoWidthMixin.__init__(self)
		
		self.Bind(wx.EVT_LIST_BEGIN_LABEL_EDIT, self.OnBeginLabelEdit)
		self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnRightClick)
		
		self.setResizeColumn(3)
		
	def OnRightClick(self, evt):
		self.ToggleItem(evt.GetIndex())
		
	def OnBeginLabelEdit(self, event):
		if event.GetColumn() == 0:
			event.Veto()
		else:
			event.Allow()

class MainWindow(wx.Frame):
	def __init__(self, *args, **kwargs):
		wx.Frame.__init__(self, *args, **kwargs)
		self.SetTitle("Host Editor")
		self.panel = wx.Panel(self)
		self.hosts_path = r"C:\Windows\System32\drivers\etc\hosts"
		
		self.list = TestListCtrl(self.panel, style=wx.LC_REPORT)
		self.list.InsertColumn(0, "OK")
		self.list.InsertColumn(1, "Domain")
		self.list.InsertColumn(2, "IP Address")
		self.list.Arrange()
		self.list.SetColumnWidth(0,30)
		self.list.SetColumnWidth(1,200)
		self.list.SetColumnWidth(2,130)
		
		self.btnAddEntry = wx.Button(self.panel, 1001, label="Add Entry")
		self.Bind(wx.EVT_BUTTON, self.OnClick, self.btnAddEntry)
		
		self.btnRemoveEntry = wx.Button(self.panel, 1002, label="Remove Entry")
		self.Bind(wx.EVT_BUTTON, self.OnClick, self.btnRemoveEntry)
		
		self.btnSave = wx.Button(self.panel, 1003, label="Save")
		self.Bind(wx.EVT_BUTTON, self.OnClick, self.btnSave)
		
		self.sizer = wx.BoxSizer(wx.VERTICAL)
		self.sizer.Add(self.btnAddEntry, flag=wx.EXPAND | wx.ALL, border=5)
		self.sizer.Add(self.btnRemoveEntry, flag=wx.EXPAND | wx.ALL, border=5)
		self.sizer.Add(self.list, proportion=1, flag=wx.EXPAND | wx.ALL, border=5)
		self.sizer.Add(self.btnSave, flag=wx.EXPAND | wx.ALL, border=5)
		self.panel.SetSizerAndFit(self.sizer)
		
		self.Show()
		
		self.app_data = os.getenv('APPDATA') + r"\HostEditor";
		isFirstRun = os.path.exists(self.app_data) == False
		
		if isFirstRun == True:
			dlg = wx.MessageBox( 'Dengan menggunakan aplikasi ini maka setting HOSTS default akan dihapus. Apakah Anda Setuju?', 'Test Dialog', wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION )
			if dlg == wx.YES:
				print('You picked yes')
			elif dlg == wx.NO:
				print('You picked no')
				self.Close()
				return
			
		if isFirstRun == True:
			open(self.hosts_path, 'w').close()
			os.makedirs(self.app_data)
			
		self.SyncRead()

	def OnClick(self, event):
		id = event.GetId()
		if event.GetId() == 1001:
			index = self.list.Append(["", "", ""])
			self.list.CheckItem(index, False)
			print("add entry")
		elif event.GetId() == 1002:
			self.list.DeleteItem(self.list.GetFocusedItem())
			print("remove entry")
		elif event.GetId() == 1003:
			self.SyncWrite()
			print("save")
	
	def SyncRead(self):
		with open(self.hosts_path) as fin:
			for line in fin:
				print(line)
				enabled = False if "#" in line else True
				print(enabled)
				
				if enabled is True:
					all = re.findall("(\S+) (\S+)", line)
					print(all)
					if all:
						ip = all[0][0]
						domain = all[0][1]
						index = self.list.Append(["", domain, ip])
						self.list.CheckItem(index, enabled)
				else:
					all = re.findall("#(\S+) (\S+)", line)
					print(all)
					if all:
						ip = all[0][0]
						domain = all[0][1]
						index = self.list.Append(["", domain, ip])
						self.list.CheckItem(index, enabled)
			fin.close()
		
	def SyncWrite(self):
		with open(self.hosts_path, 'w') as theFile:
			count = self.list.GetItemCount()
			
			for row in range(count):
				domain = self.list.GetItem(itemIdx=row, col=1).GetText()
				ip = self.list.GetItem(itemIdx=row, col=2).GetText()
				isChecked = self.list.IsChecked(row)
				
				if isChecked == True:
					theFile.write("{0} {1}\n".format(ip , domain))
				elif isChecked == False:
					theFile.write("#{0} {1}\n".format(ip , domain))
					
			theFile.close()
		
		
app = wx.App(False)
win = MainWindow(None)
app.MainLoop()